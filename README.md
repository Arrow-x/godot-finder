# <img width="48" height="48" src="https://gitlab.com/joao-pedro-braz/godot-finder/-/raw/main/icon.png"> Finder

Plugin for the Godot Game Engine that helps you find files (scenes, scripts, resources, etc) across your entire project.

![Example 1](https://gitlab.com/joao-pedro-braz/godot-finder/-/raw/main/media/example_1.png)

![Example 3](https://gitlab.com/joao-pedro-braz/godot-finder/-/raw/main/media/example_3.png)

[Demo (V2)](https://www.youtube.com/watch?v=A2vNK3Dbh_s)

[ᵈᵉᵐᵒ ᵛ¹](https://www.youtube.com/watch?v=5vlgDuNjSwA)

## Usage

To bring up the finder simply press **shift** twice (as you would in any IntelliJ based IDE) or click the search icon in the toolbar (usually the top-right corner of the editor).

Besides intuitive mouse support, the finder window also have full keyboard support:

- Arrow keys or tab/shift+tab for single-item navigation
- Pageup and Pagedown for going to the first and last item, respectively
- Enter for opening the selected file
- Ctrl + F to quickly go back to the search box

### Searching

The search functionality includes some QoL features that you should be aware:

- By default, it will use fuzzy matching, so whatever you type won't be taken literally
- Strict matching can be enabled by prepending the search with a bang ("!")
- You can search by multiple terms by separating them with a space, like "finder \_on\_clicked\_property"
- If your search term contains a forward slash ("/"), it will exclusively match against the whole file path (except for script properties). The search can be further restricted by also prepending a bang to the search. This is useful when you want to match a specific directory
- Matching can be done by intials, by simply supplying a abbreviation of what you are searching for, like "ocp", when searching for "\_on\_clicked\_property". This type of matching is done by checking for both snake_case and camelCase/PascalCase.

## Configuration

As a means of customization, you can also change some parameters in the Editor Settings, namely:

### Excluded Paths (finder/excluded_paths)

An array of directory paths you would like to exclude from the finder.  
Example:
> res://addons

### Compact Mode (finder/compact_mode)

A boolean that toggles whether the file preview should be shown in the finder.

### Smart Suggestions (finder/smart_suggestions)

A boolean that toggles whether to show a script's properties (class name, methods, variables, signals, constants and enums).

## License

This project is released under the MIT License, see [LICENSE](LICENSE) for more information.
